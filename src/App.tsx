import React from "react";
import "./App.css";
import HelloWorldClassComponent from "./components/HelloWorldClassComponent";
import HelloWorldPureComponent from "./components/HelloWorldPureComponent";
import HelloWorldFunctionalComponent from "./components/HelloWorldFunctionalComponent";
import HelloWorldCreateElement from "./components/HelloWorldCreateElement";

function App() {
  return (
    <div className="App">
      <main>
        {HelloWorldCreateElement}
        <HelloWorldClassComponent />
        <HelloWorldPureComponent text="additional text" />
        <HelloWorldFunctionalComponent />
      </main>
    </div>
  );
}

export default App;
