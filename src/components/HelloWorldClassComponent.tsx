import React, { Component } from "react";

export default class HelloWorldClassComponent extends Component {
  render() {
    return <h1>Hello World - Class component</h1>;
  }
}
