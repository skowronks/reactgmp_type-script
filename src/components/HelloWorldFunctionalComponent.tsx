import React, { FunctionComponent } from "react";

const HelloWorldFunctionalComponent: FunctionComponent = () => (
  <h1>Hello world - Functional component</h1>
);
export default HelloWorldFunctionalComponent;
