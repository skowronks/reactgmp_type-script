import React, { PureComponent } from "react";

interface pureComponentProps {
  text: string;
}
export default class HelloWorldPureComponent extends PureComponent<
  pureComponentProps
> {
  constructor(props: pureComponentProps) {
    super(props);
  }

  render() {
    const { text } = this.props;
    return <h1>Hello World - Pure Component: {text} </h1>;
  }
}
